/**
 * Created by lokeshagrawal on 11/05/17.
 */
import React, {Component} from 'react';

// to connect to redux
import { connect } from 'react-redux';

import { bindActionCreators } from 'redux';
import { fetchWeather } from '../actions/index';

class SearchBar extends Component {
    constructor(props) {
        super(props);

        // this is a component state, which is different from the application state.
        this.state = {term: ''};

        // What is line below doing:
        // sort of overriding a local method:
        // see RHS, this refers to an instance of SearchBar, has a function called onInputChange
        // bind that function to 'this' which is a SearchBar instance. and replace an existing function onInputChange
        // with this bound function.
        this.onInputChange = this.onInputChange.bind(this);
        this.onFormSubmit = this.onFormSubmit.bind(this);
    }

    onInputChange(event) {
        this.setState({term: event.target.value});
    }

    // demo this without having this and pressing enter and clicking enter on a button, page refresh and content gone from the input box.
    onFormSubmit(event) {
        // stop form submission here
        event.preventDefault();

        // we need to go and fetch weather data
        this.props.fetchWeather(this.state.term);

        // to clear the input when search query is submitted
        this.setState({ term: ''});
    }

    // remember whenever u tap enter on a input, inside form or click submit button inside form,
    // form gets submitted, thats a default behavior. Hence bind an event to a form and control auto submission of the form.

    // remember whenever we hand a callback function like this off and then the callback references this, this is
    // going to have an incorrent context, its going to have some mistery context.

    // Why we need form for this usecase, if we are overriding the default sumbit functionality. We get few things for free with Form,
    // on enter key press search should happen, which comes handy with form, we dont have to right an extra event handling for this.
    render() {
        return (
            <form onSubmit={this.onFormSubmit} className="input-group">
                <input
                    placeholder="Get a 5 days forecast in your favorite city"
                    className="form-control"
                    value={this.state.term}
                    onChange={this.onInputChange}
                />
                <span className="input-group-btn">
                    <button type="submit" className="btn btn-secondary">Submit</button>
                </span>
            </form>
        );
    }
}

// This causes an action creator, whenever it gets called and returns an action, find action creators with dispatch.
// Make sure that action flows down into the middleware and then the reducers inside of our redux application.
function mapDispatchToProps(dispatch) {
    return bindActionCreators({ fetchWeather }, dispatch);
}

// to connect to redux
// Previously we had a components or containers where we map dispatch to props and we map state to props as well.
// So the only reason we are passing null in here is that whenever we are passing in a function that is supposed to
// map our dispatch to the props of our container it always goes in as the second argument. Passing in null in the first argument
// is just saying that hey you know I understand the redux is maintaining some state but this container doesnt care about it all. Thanks but we dont need state over here.
export default connect(null, mapDispatchToProps)(SearchBar);