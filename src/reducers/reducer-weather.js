/**
 * Created by lokeshagrawal on 12/05/17.
 */
import { FETCH_WEATHER } from '../actions/index';

// array because we will be keep searching for cities and we dont want to delete the last searched cities.
export default function(state = [], action){
    console.log('Action Received', action);
    switch(action.type){
        case FETCH_WEATHER:
            //return [ action.payload.data ]; // Cant do this because it will override the data of the cities collected before.
            //return state.push(action.payload.data) ; // Dont do this either, we should never manipulate state object directly. Remember in react we did setState not state = something. This is a mutating of an array inplace. we should not mutate a state, we should return a completely new instance of state.

            //return state.concat([action.payload.data]); // We can do this, concat doesnt change the existing array, it creates a new array old stuff + new stuff. will look like { city, city, city} NOT [city, [city, city]]
            return [ action.payload.data, ...state];  // ES6 way of concatinating stuff.
    }

    return state;
}
