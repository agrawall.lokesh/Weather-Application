/**
 * Created by lokeshagrawal on 12/05/17.
 */

import React, { Component } from 'react';

class GoogleMap extends Component {
    componentDidMount() {

        // google.maps is coming from index.html line <script src="https://maps.googleapis.com/maps/api/js"></script>
        // This is how we create an embedded google map inside our document.
        // Takes a reference to an HTML Node where we want to render this map to
        // second argument is an optional object
        new google.maps.Map(this.refs.map, {
            zoom: 12,
            center: {
                lat: this.props.lat,
                lng: this.props.lon
            }
        });
    }


    render() {

        // Makes use of refs system in react.
        // Ref System allows us to get a reference to an html element
        // that has referenced to a page
        // so after this component has been rendered to a screen, I can get a direct
        // reference to a div that was created right here by refering to
        // this.refs.map, so anywhere in this component I can say this.refs.map
        // and it will give me a direct reference to this actual html element.

        // this is how we make third parties work in our react project without them being react aware.
        return <div ref="map" />;
    }
}

export default GoogleMap;

/**
React Way:

import React from 'react';
import { GoogleMapLoader, GoogleMap} from 'react-google-maps';

export default (props) => {
    return {
        <GoogleMapLoader
            containerElement={<div style={{height: '100%'}}/>}
            googleMapElement={<GoogleMap defaultZone={12} defaultCenter={{lat: props.lat, lng: props.lon}}/>}
        />
    }
}
 **/