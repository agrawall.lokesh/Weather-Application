// This file contains all action creators.

// npm install --save axios
import axios from 'axios';

const API_KEY = 'ef04def44ffe0b6025fd9d504c293f4e';

// Ask them to visit http://openweathermap.org/forecast5 to see what all APIs are available to get data,
// JSON DATA http://samples.openweathermap.org/data/2.5/forecast?q=London,us&appid=b1b15e88fa797225412429c1c50c122a1
// XML DATA http://samples.openweathermap.org/data/2.5/forecast?q=London,us&mode=xml&appid=b1b15e88fa797225412429c1c50c122a1

// Template String
const ROOT_URL = `http://api.openweathermap.org/data/2.5/forecast?appid=${API_KEY}`;

// export because we will use this const in reducers.
export const FETCH_WEATHER = 'FETCH_WEATHER';

export function fetchWeather(city) {
    const url = `${ROOT_URL}&q=${city},in`;

    // axios returns a promise, so request object here is a promise object.
    const request = axios.get(url);

    // check what gets printed here and check what gets printed from reducer, to know the use of middleware.
    console.log('Request is: ', request);

    // this is an action object
    // see we are returning the promise as the payload.
    return {
        type: FETCH_WEATHER,
        payload: request
    };
}
