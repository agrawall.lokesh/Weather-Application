/**
 * Created by lokeshagrawal on 12/05/17.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';
import Chart from '../components/chart';
import GoogleMap from '../components/google_map'

class WeatherList extends Component {

    renderWeather(cityData) {
        const name = cityData.city.name;
        const temps = cityData.list.map(weather => weather.main.temp);
        //  for degree celsius, const temps = _.map(cityData.list.map(weather => weather.main.temp), (temp => temp - 273));
        const pressures = cityData.list.map(weather => weather.main.pressure);
        const humidities = cityData.list.map(weather => weather.main.humidity);

        /*
        const lon = cityData.city.coord.lon;
        const lat = cityData.city.coord.lat;
        */

        // ES6 way for above two commented lines
        const { lon, lat } = cityData.city.coord;

        return (
            <tr key={name}>
                <td><GoogleMap lon={lon} lat={lat} /></td>
                <td><Chart data={temps} color="purple" units="K"/></td>
                <td><Chart data={pressures} color="green" units="hPa"/></td>
                <td><Chart data={humidities} color="black" units="%"/></td>
            </tr>
        )
    }

    render(){
        return (
            <table className="table table-hover">
                <thead>
                <tr>
                    <th> City </th>
                    <th> Temperature (K)</th>
                    <th> Pressure (hPa)</th>
                    <th> Humidity (%)</th>
                </tr>
                </thead>
                <tbody>
                    {this.props.weather.map(this.renderWeather)}
                </tbody>
            </table>
        )
    }
}

/**
function mapStateToProps(state) {
    return {
        weather:state.weather
    };
}
 **/

// ES6 way of writing above method.
function mapStateToProps({weather}){
    return{weather}; // {weather} === {weather: weather}
}

// connect state with current component
export default  connect(mapStateToProps)(WeatherList);